#!/bin/python2

def main():
   import logging

   #enable logging
   logging.basicConfig(level=logging.INFO)
   logging.info('Starting stop_servers...')

   import projects
   import openstack

   openstack.destroy_unused_servers()
   # write temp_data
   projects.write_temp_data()
   logging.info('Finished!')


if __name__ == '__main__':
    main()
