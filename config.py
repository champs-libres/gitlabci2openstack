import argparse
import logging

logger = logging.getLogger('config')

parser = argparse.ArgumentParser()
parser.add_argument("configdir", help="directory which contains project.yml and openstack.yml files")
args = parser.parse_args()

configdir = args.configdir
logger.debug('config directory is %s', configdir)