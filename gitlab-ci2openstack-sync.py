#!/bin/python2
import config

def main():
   import logging

   #enable logging
   logging.basicConfig(level=logging.INFO)
   logging.info('Starting gitlab-ci2openstack sync...')

   import projects
   import openstack

   #perform action
   for project_name in projects.list_projects():
      logging.info("performing on project %s", project_name)
      pendings = projects.get_pending_builds(project_name)
      if (len(pendings) > 0):
         logging.info('I found pending builds')
         for build in pendings:
            server_name = projects.get_server_name_for_build(project_name, build)
            openstack.set_server_required(server_name)
      else:
         logging.info('No pending builds !')
   openstack.start_required_servers()
   openstack.destroy_unused_servers()
   # write temp_data
   projects.write_temp_data()
   logging.info('Finished!')


if __name__ == '__main__':
    main()
