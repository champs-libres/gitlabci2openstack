
import yaml
import requests
import logging
import config

TEMP_FILE = '/tmp/projects.tmp.yaml'

logger = logging.getLogger('projects')

#import config
f = open(config.configdir+'/projects.yml');
data = yaml.safe_load(f)
logger.info('Loading configuration from projects.yml')
logger.debug('configuration is %s', data)
f.close()

#import temp data
# some elements are stored in a temporary file to avoid unecessary lookups
try:
   f = open(TEMP_FILE)
   logger.info('temp data found and imported')
except IOError:
   # create the file and open it
   logger.info('No temp data file found, creating temp data at '+TEMP_FILE)
   g = open(TEMP_FILE, 'a')
   g.close()
   f = open(TEMP_FILE)
finally:
   temp_data = yaml.safe_load(f)
   f.close()
   # populate data if not exists
   # temp is None ? We must populate for each project
   if temp_data == None:
      first_opening = True
      temp = {}
   else:
      first_opening = False
      temp = temp_data

def get_data(project_name):
   """
      return a dictionary with all data about project.
      this include data stored in config (project.yml) AND temporary data
   """
   if project_name not in data:
      raise Exception('the project is not in the data in projects.yml')
   project_data = data[project_name]
   temp_data = _get_temp_data(project_name)
   project_data.update(temp_data)
   return project_data

def list_projects():
   """
      return a generator of projects name
   """
   for project_name, project in data.iteritems():
      yield project_name

def _get_project_name_by_id(project_id):
   """
      return the project name for a given project id
   """
   for project_name, project in data.iteritems():
      if project["id"] == project_id:
         return project_name

def get_pending_builds(project_name):
   data = get_data(project_name)
   must_go_further = True
   per_page = 10
   response_list = []
   while must_go_further:
      arguments = { "project_token" : data["project_token"], "project_id" : data["project_id"], "per_page" : per_page, "page": data["get_commit_page"] }
      r = requests.get(data["base_url"] + "/api/v1/commits", params=arguments)
      response = r.json()
      logger.debug('request to %s has response : %s', r.url, r.json())
      response_list.append(response)
      if len(response) < per_page:
         must_go_further = False
      else:
         data["get_commit_page"] = data["get_commit_page"] + 1
         # upgrade temp
         temp[project_name]["get_commit_page"] = data["get_commit_page"]
   #processing responses
   pendings = []
   for response in response_list:
      for commit in response:
         for build in commit["builds"]:
            if build["status"] in ["pending", "running"]:
               logger.info("build %s is pending or running for project %s since %s", build["id"], build["project_id"], build["created_at"])
               pendings.append(build)
   return pendings

def get_server_name_for_build(project_name, build):
   data = get_data(project_name)
   if build['runner_id'] is None:
      logger.warning('The runner for task %s (project %s) is not defined, using default runner (under _default key)', str(build['id']), project_name)
      return data['runners']['_default']
   print(build)
   if build['runner_id'] not in data['runners']:
      logger.error("The runner with id %s is not configured in project %s", str(build['runner_id']), project_name)
      raise Exception("The runner is not configured. See logs for details")
   return data['runners'][build['runner_id']]
         

def write_temp_data():
   f = open(TEMP_FILE, 'w')
   yaml.dump(temp, f)
   f.close()

def _get_empty_temp_data():
   return {
            'get_commit_page' : 0
            }

def _get_temp_data(project_name):
   if project_name not in list(temp.keys()) :
      temp[project_name] = _get_empty_temp_data()
   return temp[project_name]


