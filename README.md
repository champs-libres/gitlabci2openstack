Gitlab-ci2openstack
===================

This project provides script to run gitlab-ci on servers run through openstack.

The scripts detects if build are pending in gitlab-ci and, if any, start a server to run the builder and execute test. A couple of minutes later, the script stops the server if it isn't required.

The script should be run with a cronjob.

What does this script ?
-----------------------

On each execution, the script `gitlab-ci2openstack`: 

- check if there are pending builds associated with the project. Only the last page, containing at least the 10 last project, will be checked (amongst our todo we have to configure this and to go ahead on older commits) ;
- if there are some builds pending, start a server according to the configuration ;
- if there are servers started without any job (which appears when a server has been started during a previous execution of the script, and which have finished his job), shutdown and delete the server.

Prerequisites
-------------

- python 2
- [openstack nova client library](http://docs.openstack.org/developer/python-novaclient/index.html) (on Debian-like, package `python-novaclient`)

Preparation
-------------

**Prepare an image** which contains your runner. Usually, you should start a server manually, [install gitlab-ci-multi-runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner#installation) on this server and save the server as an image.

**Prepare your configuration**, eventually using the command `python list_openstack_capabilities.py` which will list all required parameters with the id (flavors, images, keypairs, ...). The image you saved in the previous steps should appears in the listed images.

Currently, the configuration is in two files and **must be place in the same directory as the other scripts** : 

- `projects.yml` (example in `projects.yml.dist`)
- `openstack.yml` (example in `openstack.yml.dist`)

To list your projects with their id, you can use the gitlab CI api with your personal auth token :

```
curl --header "PRIVATE-TOKEN: your-token-here" https://url.to.git.lab/ci/api/v1/projects | python -m json.tool
```

Your personal token is available at `https://url.to.git.lab/profile/account`.

You can run the same runner to multiples project via api : 

```
$ curl --header "PRIVATE-TOKEN: your-token" -X POST https://git.framasoft.org/ci/api/v1/projects/:project_id/runners/:runner_id
```

Currently, the only way to get the runner id is to see the id (`#id`) next to the runner name at the page https://ur.to.git.lab/yourname/yourproject/runners 

**Note that the user which run scripts should be able to write in /tmp directory**. Some temporary file are written down there.

Running
-------

Simply run the script `python gitlab-ci2openstack-sync.py`.

You can run the task from an cron job (use `/usr/bin/python` instead of `python`).

Stopping servers
-----------------

If you run the script only during part of the day, you should stop servers at the end of the day **without to checking for pending builds**. This ensure that, if pending build are runned at the end of the day, the servers remains started (and you pay for it) until the next morning.

Run `python stop_servers.py`.

