from novaclient import client
import yaml
import logging
import config

class Factory: 

   def __init__(self):
      self.logger = logging.getLogger('openstack.Factory')
      #import config
      try:
         f = open(config.configdir + '/openstack.yml');
      except Exception:
         message = 'The file openstack.yml could not be opened. Did you created a configuration file ?'
         self.logger.error(message)
         print(message)
      self.config = yaml.safe_load(f)
      logging.debug('Loading configuration from ovh.yml')
      f.close()
      #create a nova clienet
      self.nova_client = client.Client(2, self.config['username'], self.config['password'], self.config['tenant']['name'], self.config['auth_url'], service_type='compute', tenant_id=self.config['tenant']['id'], region_name=self.config['region'])

   def get_server_definition(self, server_name):
      if server_name not in self.config["servers"]:
         self.logger("The server %s is not defined", server_name)
         raise Exception("The server is not defined")
      s = self.config["servers"][server_name]
      s["name"] = server_name
      return s
   
   def get_server_names(self):
      return self.config["servers"].keys()

factory = Factory()
logger = logging.getLogger('openstack')
running_servers = []
required_servers = []

def _cache_running_servers():
   """
   store running servers into cache
   """
   for server in factory.nova_client.servers.list():
      logger.info("found running server : %s", server.name)
      running_servers.append(server.name)

def _is_server_running(server_name):
   if len(running_servers) == 0:
      _cache_running_servers()
   return server_name in running_servers

def destroy_unused_servers():
   """
      destroy all servers
   """
   for server in factory.nova_client.servers.list():
      logger.info("found running server : %s", server.name)
      if server.name in factory.get_server_names():
         if server.name not in required_servers:
            logger.info("request to stop server %s", server.name)
            server.delete()

def set_server_required(server_name):
   """
   set a server as required. the server will be called on the
   next call to start_required_servers
   """
   factory.logger.info("required to start server %s", server_name)
   required_servers.append(server_name)
   
def start_required_servers():
   """
   start all servers required
   """
   for server_name in required_servers:
      start_server(server_name)

def start_server(server_name):
   """
   start a server if required
   """
   if _is_server_running(server_name):
      logger.info("server %s already started", server_name)
   else:
      s = factory.get_server_definition(server_name)
      logger.warning("creating a server with the first keypair in the list - multiple keys is not yet implemented")
      block_dev_mapping = {}
      if 'block_volumes' in s:
         for mountpoint, volume_id in s['block_volumes'].iteritems():
            block_dev_mapping[mountpoint] = volume_id
      s = factory.nova_client.servers.create(s["name"], s["image_id"], s["flavor_id"], key_name=s["keypairs"][0],
                                             block_device_mapping=block_dev_mapping)
      logger.info("creating server %s at %s", s.name, s.created)
      running_servers.append(s.name)

