import logging
import openstack


logger = logging.getLogger('list_openstack_capabilities')

factory = openstack.factory

logging.debug("Begin printing of capabilities")

print("list of available flavors")
print("")

for flavor in factory.nova_client.flavors.list():
   print(" id: "+flavor.id + " name: " + flavor.name)

print("")
print("list of available images")
print("")

for i in factory.nova_client.images.list():
   print(" id: "+ i.id + " name : "+ i.name)   

print("")
print("list of keypairs")
print("")

keypairs = factory.nova_client.keypairs.list()
if len(keypairs) == 0:
   print(" no keypairs")
else:
   for k in keypairs:
      print(" id: " + k.id + " name: "+ k.name)

print("")
print("list of running servers")
print("")

servers = factory.nova_client.servers.list()
if len(servers) == 0:
   print(" no servers running")
else:
   for s in servers:
      print(" name: "+ s.name + " created at " + s.created + " status: " + s.status + ", last updated: " + s.updated)

logging.debug("End of printing capabilities")
